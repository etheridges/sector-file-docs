# /Airports/EGxx/Basic.txt
# What are they?
The basic definition of an airport including center point (used by .center) and base frequency (unused). 

# How to format them
The source Basic.txt definition (found at Airports/EGNX/Basic.txt) is defined as below:

    <airport name>
    <Coordinates>
    <primary approach frequency>
   E.g. 

    East Midlands
    N052.49.52.000 W001.19.41.000
    119.650

The coordinates follow standard EuroScope coordinate formatting.

# Where to find them
Position information can be found by searching the .SCT file for the airport ICAO. E.g. 'EGNX'
